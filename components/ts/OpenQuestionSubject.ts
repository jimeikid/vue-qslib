import BaseQuestionSubject from './TQuestionSubject';
import BaseOption from './BaseOption';

export class OpenQuestionOption extends BaseOption {

}
export default class OpenQuestionSubject extends BaseQuestionSubject<OpenQuestionOption> {
    public static option: OpenQuestionOption;

    public QuestionText: string = '';

}
